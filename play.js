class play extends Phaser.Scene {
    constructor() {
        super("playGame");
    }

    addImages() {
        // adding, resizing, and positioning visual assets (x, y, image)
        
        // CORE ASSETS (with physics)
        // background
        this.background = this.add.image(config.width/2, -3000, "background")
            .setScale(2);
        // mountain
        this.mountain = this.add.image(config.width/2, 290, "mountain");
        // ground
        this.ground = this.add.image(config.width/2, 320, "ground");
        // left flame
        this.firstFlame = this.physics.add.image(config.width/2-15, config.height-25, "flame")
            .setScale(0.2)
            .setVisible(false);
        // right flame
        this.secondFlame = this.physics.add.image(config.width/2+15, config.height-25, "flame")
            .setScale(0.2)
            .setVisible(false);
        // astronaut
        this.astronaut = this.physics.add.image(config.width/2, config.height-60, "astronaut")
            .setScale(0.6)
            .setImmovable(true);

        // HEALTH BAR
        // the actual health bar (red rectangle)
        this.healthBar = this.makeBar(724, 30, 0xff0000, 255, 27);
        this.scaleHealthBar(this.healthBar, 100);
        // the health bar border (for decoration)
        this.healthBarBorder = this.add.image(config.width-170, 70, "health-bar-border")
            .setScale(0.3);

        //OBSTACLES (with physics)
        // crow from left
        this.crowLeft = this.physics.add.image(config.width+170, Phaser.Math.Between(0, config.height), "crow")
            .setScale(0.2)
            .setVisible(false);
        // crow from right
        this.crowRight = this.physics.add.image(-170, Phaser.Math.Between(0, config.height), "crow")
            .setScale(0.2)
            .setVisible(false);
        // goose from left
        this.gooseLeft = this.physics.add.image(config.width+170, Phaser.Math.Between(0, config.height), "goose")
            .setScale(0.1)
            .setVisible(false);
        // goose from right
        this.gooseRight = this.physics.add.image(-170, Phaser.Math.Between(0, config.height), "goose")
            .setScale(0.1)
            .setVisible(false);
        // helicopter from left
        this.helicopterLeft = this.physics.add.image(config.width+170, Phaser.Math.Between(0, config.height), "helicopter")
            .setScale(0.2)
            .setVisible(false);
        // helicopter from right
        this.helicopterRight = this.physics.add.image(-170, Phaser.Math.Between(0, config.height), "helicopter")
            .setScale(0.2)
            .setVisible(false);
        // aeroplane from left
        this.aeroplaneLeft = this.physics.add.image(config.width+170, Phaser.Math.Between(0, config.height), "aeroplane")
            .setScale(0.4)
            .setVisible(false);
        // aeroplane from right
        this.aeroplaneRight = this.physics.add.image(-170, Phaser.Math.Between(0, config.height), "aeroplane")
            .setScale(0.4)
            .setVisible(false);
        // fighter jet from left
        this.fighterJetLeft = this.physics.add.image(config.width+170, Phaser.Math.Between(0, config.height), "fighter-jet")
            .setScale(0.25)
            .setVisible(false);
        // fighter jet from right
        this.fighterJetRight = this.physics.add.image(-170, Phaser.Math.Between(0, config.height), "fighter-jet")
            .setScale(0.25)
            .setVisible(false);
        // rocket from left
        this.rocketLeft = this.physics.add.image(config.width+170, Phaser.Math.Between(0, config.height), "rocket")
            .setScale(0.25)
            .setVisible(false);
        // rocket from right
        this.rocketRight = this.physics.add.image(-170, Phaser.Math.Between(0, config.height), "rocket")
            .setScale(0.25)
            .setVisible(false);
        // satellite from left
        this.satelliteLeft = this.physics.add.image(config.width+170, Phaser.Math.Between(0, config.height), "satellite")
            .setScale(0.25)
            .setVisible(false);
        // satellite from right
        this.satelliteRight = this.physics.add.image(-170, Phaser.Math.Between(0, config.height), "satellite")
            .setScale(0.25)
            .setVisible(false);
        // ufo from left
        this.ufoLeft = this.physics.add.image(config.width+170, Phaser.Math.Between(0, config.height), "ufo")
            .setScale(0.25)
            .setVisible(false);
        // ufo from right
        this.ufoRight = this.physics.add.image(-170, Phaser.Math.Between(0, config.height), "ufo")
            .setScale(0.25)
            .setVisible(false);
        // heart
        this.heart = this.physics.add.image(Phaser.Math.Between(22, config.width-22), -50, "heart")
            .setScale(0.075);
    }

    addText() {
        // score text top-left
        this.scoreText = this.add.text(20, 20, this.score + " ft", {
            font: "25px Arial",
            fill: "yellow"
        });
        // countdown text
        this.countdownText = this.add.text(config.width/2-40, config.height/4.5, "3", {
            font: "200px Arial",
            fill: "yellow"
        });
        // menu countdown text
        this.menuCountdownText = this.add.text(config.width/2-130, config.height/1.3, "", {
            font: "25px Arial",
            fill: "#00ff00"
        }).setVisible(false);
    }

    // COLLIDERS
    addColliders() {
        // CROW
        this.physics.add.collider(this.astronaut, this.crowLeft, () => {
            this.resetLeft(this.crowLeft); 
            this.subtractHealth(10);
        });
        this.physics.add.collider(this.astronaut, this.crowRight, () => {
            this.resetRight(this.crowRight);
            this.subtractHealth(10);
        });
        // GOOSE
        this.physics.add.collider(this.astronaut, this.gooseLeft, () => {
            this.resetLeft(this.gooseLeft);
            this.subtractHealth(10);
        });
        this.physics.add.collider(this.astronaut, this.gooseRight, () => {
            this.resetRight(this.gooseRight);
            this.subtractHealth(10);
        });
        // HELICOPTER
        this.physics.add.collider(this.astronaut, this.helicopterLeft, () => {
            this.resetLeft(this.helicopterLeft);
            this.subtractHealth(10);
        });
        this.physics.add.collider(this.astronaut, this.helicopterRight, () => {
            this.resetRight(this.helicopterRight);
            this.subtractHealth(10);
        });
        // AEROPLANE
        this.physics.add.collider(this.astronaut, this.aeroplaneLeft, () => {
            this.resetLeft(this.aeroplaneLeft);
            this.subtractHealth(12);
        });
        this.physics.add.collider(this.astronaut, this.aeroplaneRight, () => {
            this.resetRight(this.aeroplaneRight);
            this.subtractHealth(12);
        });
        // FIGHTER JET
        this.physics.add.collider(this.astronaut, this.fighterJetLeft, () => {
            this.resetLeft(this.fighterJetLeft);
            this.subtractHealth(14);
        });
        this.physics.add.collider(this.astronaut, this.fighterJetRight, () => {
            this.resetRight(this.fighterJetRight);
            this.subtractHealth(14);
        });
        // ROCKET
        this.physics.add.collider(this.astronaut, this.rocketLeft, () => {
            this.resetLeft(this.rocketLeft);
            this.subtractHealth(16);
        });
        this.physics.add.collider(this.astronaut, this.rocketRight, () => {
            this.resetRight(this.rocketRight);
            this.subtractHealth(16);
        });
        // SATELLITE
        this.physics.add.collider(this.astronaut, this.satelliteLeft, () => {
            this.resetLeft(this.satelliteLeft);
            this.subtractHealth(20);
        });
        this.physics.add.collider(this.astronaut, this.satelliteRight, () => {
            this.resetRight(this.satelliteRight);
            this.subtractHealth(20);
        });
        // UFO
        this.physics.add.collider(this.astronaut, this.ufoLeft, () => {
            this.resetLeft(this.ufoLeft);
            this.subtractHealth(25);
        });
        this.physics.add.collider(this.astronaut, this.ufoRight, () => {
            this.resetRight(this.ufoRight);
            this.subtractHealth(25);
        });
        // HEART
        this.physics.add.collider(this.astronaut, this.heart, () => {
            this.heart.x = Phaser.Math.Between(22, config.width-22);
            this.heart.y = -50;
            this.heart.setVelocity(0,0);
            this.addHealth(10);
        });
    }

    makeBar(x, y, colour, width, height) {
        // instantiate the bar
        let bar = this.add.graphics();
        // assign colour to bar
        bar.fillStyle(colour, 1);
        // fill bar with rectangle
        bar.fillRect(0, 0, width, height);
        // position bar
        bar.x = x;
        bar.y = y;
        // returns health bar graphic to function call
        return bar;
    }

    addHealth(value) {
        if (this.gameOver == true) {
            return;
        }
        // if health is equal to 100 then avoid running rest of function; do nothing
        if (this.health >= 100) {
            this.health = 100;
            this.addition = this.makeBar(724, 30, 0x00ff00, 255, 27);
            this.healthBarBorder.depth = 100;
            setTimeout(() => {this.addition.destroy();}, 150);
            return;
        }
        // if an addition indicator bar exists, delete it
        if (this.addition) {
            this.addition.destroy();
        }
        // increases health value by specified value parameter
        this.health += value;
        // calculates the X value for the end of the health bar
        let endOfHealth = 724+(255*this.healthBar.scaleX);
        // calculates the size of the addition indicator bar
        let sizeOfAddition = 255*(value/100);
        // calculates a max addition value to avoid appearing beyond health bar border
        let maxValue = 979-endOfHealth;
        // if size of addition is greater than max value, reassign max value to size of addition
        if (sizeOfAddition > maxValue) {
            sizeOfAddition = maxValue;
        }
        // create addition indicator bar
        this.addition = this.makeBar(endOfHealth, 30, 0x00ff00, sizeOfAddition, 27);
        // re-add border so that it is brought to front (layered on top of green addition bar)
        this.healthBarBorder.depth = 100;
        // delete addition indicator bar after 150 milliseconds (0.15 seconds)
        setTimeout(() => {this.addition.destroy();}, 150);
        // if health is greater than 100 then reset to 100 to avoid going over max health value of 100
        if (this.health > 100) {
            this.health = 100;
        }
        // scale health bar with current health value
        this.scaleHealthBar(this.healthBar, this.health);
    }
    
    subtractHealth(value) {
        if (this.gameOver == true) {
            return;
        }
        if (this.health <= 0) {
            this.health = 0;
            return;
        }
        // if a subtraction indicator bar exists, delete it
        if (this.subtraction) {
            this.subtraction.destroy();
        }
        // decreases health value by specified value parameter
        this.health -= value;
        // scale health bar with new health value
        this.scaleHealthBar(this.healthBar, this.health);
        // calculates the X value for the end of the health bar
        let endOfHealth = 724+(255*this.healthBar.scaleX);
        // calculates the size of the subtraction indicator bar
        let sizeOfSubtraction = 255*(value/100);
        // if end of health bar is less than starting point of end bar (it's protruding backwards)
        if (endOfHealth < 724) {
            // calculates new size of subtraction
            sizeOfSubtraction -= 724-endOfHealth;
            // set end of health bar to original starting location
            endOfHealth = 724;
        }
        // create subtraction indicator bar
        this.subtraction = this.makeBar(endOfHealth, 30, 0xffff00, sizeOfSubtraction, 27);
        // re-add border so that it is brought to front (layered on top of yellow subtraction bar)
        this.healthBarBorder.depth = 100;
        // delete subtraction indicator bar after 150 milliseconds (0.15 seconds)
        setTimeout(() => {this.subtraction.destroy();}, 150);
        // if health is less than or equal to zero then game over and return
        if (this.health <= 0) {
            this.health = 0;
            this.gameOver = true;
        }
        // scale health bar with current health value
        this.scaleHealthBar(this.healthBar, this.health);
    }

    scaleHealthBar(bar, percentage) {
        // scale width of bar based on percentage
        bar.scaleX = percentage/100;
    }

    updateCountdown() {
        // minus 1 from counter
        this.counter--;
        // control countdown text
        if (this.counter > 0) {
            this.countdownText.setText(this.counter);
        } else if (this.counter == 0 || this.counter == -1) {
            this.countdownText.setText("Lift Off!").x = config.width/2-325;
        } else {
            this.countdownText.setVisible(false);
        }
    }

    moveCharacter() {
        // variables for horizontal and vertical velocity
        this.xVelocity = 0;
        this.yVelocity = 0;
        // LEFT
        if (this.keys.left.isDown) {
            // set bound for left edge of canvas
            if (this.astronaut.x < 30) {
                // do nothing
            } else {
                this.xVelocity = -this.characterSpeed;
            }

        // RIGHT
        } if (this.keys.right.isDown) {
            // set bound for right edge of canvas
            if (this.astronaut.x > config.width - 30) {
                // do nothing
            } else {
                this.xVelocity = this.characterSpeed;
            }
 
        // UP
        } if (this.keys.up.isDown) {
            // set bound for top edge of canvas
            if (this.astronaut.y < 40) {
                // do nothing
            } else {
                this.yVelocity = -this.characterSpeed;
            }
  
        // DOWN
        } if (this.keys.down.isDown) {
            // set bound for bottom edge of canvas
            if (this.astronaut.y > config.height - 50) {
                // do nothing
            } else {
                this.yVelocity = this.characterSpeed;
            }
        }
        // set velocity based on keys pressed
        this.astronaut.setVelocity(this.xVelocity,this.yVelocity);
        this.firstFlame.setVelocity(this.xVelocity,this.yVelocity);
        this.secondFlame.setVelocity(this.xVelocity,this.yVelocity);
    }

    // assigns visibility and velocity to an obstacle
    obstacleVelocity(obstacle, speed) {
        obstacle.setVisible(true);
        if (speed > 0) {
            // assign X speed and random downwards speed
            obstacle.setVelocity(speed, this.randomSpeedOne);
        } if (speed < 0) {
            // assign X speed and random downwards speed
            obstacle.setVelocity(speed, this.randomSpeedTwo);
        }
        // if obstacle leaves the canvas (left edge, right edge, bottom, top edge)
        if (obstacle.x < -170 || obstacle.x > config.width+170 || obstacle.y < -50 || obstacle.y > config.height+50) {
            // reset obstacle position
            this.resetObstaclePosition(obstacle);
        }  
    }

    // reset to left edge
    resetLeft(obstacle) {
        obstacle.x = -170;
        obstacle.y = this.randomY;
        this.randomSpeedOne = Phaser.Math.Between(-150, 350);
    }

    // reset to right edge
    resetRight(obstacle) {
        obstacle.x = config.width+170;
        obstacle.y = this.randomY;
        this.randomSpeedTwo = Phaser.Math.Between(-150, 350);
    }

    // resets an obstacles position based on horizontal velocity
    resetObstaclePosition(obstacle) {
        // random y value between top and bottom of canvas
        this.randomY = Phaser.Math.Between(0, config.height);
        // if obstacles is moving right
        if (obstacle.body.velocity.x > 0) {
            this.resetLeft(obstacle);
        // if obstacles is moving left
        } if (obstacle.body.velocity.x < 0) {
            this.resetRight(obstacle);
        // do nothing
        } else {
            return;
        }
    }

    spawnHeart() {
        this.heart.x = Phaser.Math.Between(22, config.width-22);
        this.heart.y = -50;
        this.heart.setVelocity(0, 400);
    }

    returnToMenu() {
        this.menuCountdownText.setText("Returning to menu in " + this.menuCountdown);
        // decrement menu countdown by 1
        this.menuCountdown--;
        // menu countdown text
        if (this.menuCountdown == -1) {
            this.scene.start("bootGame");
        }
    }

    // MAIN FUNCTIONS
    create() {
        this.gameOver = false;
        this.score = 0;
        this.counter = 3;
        this.menuCountdown = 20;
        this.menuCountDownInitiated = false;
        // health value
        this.health = 100;
        // initial random values for both randomSpeed variables
        this.randomSpeedOne = Phaser.Math.Between(-500, 500);
        this.randomSpeedTwo = Phaser.Math.Between(-500, 500);
        
        // add image and text elements to canvas
        this.addImages();
        this.addText();
        this.addColliders();

        // countdown event repeats every second for a total of 4 times
        this.time.addEvent({delay: 1000, callback: this.updateCountdown, callbackScope: this, repeat: 4});
        // initialise left and right arrow key inputs
        this.keys = this.input.keyboard.addKeys('left, right, up, down');

        setTimeout(() => 
        {this.time.addEvent({delay: 2500, callback: this.spawnHeart, callbackScope: this, loop: true});}
        ,10000);

        this.time.addEvent({delay: 1000, callback: this.returnToMenu, callbackScope: this, loop: true});

        this.characterSpeed = 650;
    }

    update() {
        // once countdown reaches 0 begin ascending
        if (this.counter <= 0 && this.counter > -3) {
            // initiate camera scroll
            //camera.scrollY -= speed;
            this.background.y += 1.6;
            this.mountain.y += 0.8;
            this.ground.y += 1;

            // increment score in ft
            this.score+=80;
            // update score text
            this.scoreText.setText(this.score + " ft");
        }

        // begin astronaut ascent
        if (this.counter == 0 || this.counter == -1) {
            // move astronaut upwards slightly
            this.astronaut.y -= 0.5;
            // turn jetpack on visually
            this.firstFlame.setVisible(true).y -= 0.5;
            this.secondFlame.setVisible(true).y -= 0.5;
        }

        // FLIP X to reverse images
        this.crowRight.flipX = true;
        this.gooseRight.flipX = true;
        this.helicopterRight.flipX = true;
        this.aeroplaneLeft.flipX = true;
        this.fighterJetLeft.flipX = true;
        this.rocketRight.flipX = true;
        this.satelliteRight.flipX = true;


        // once countdown complete
        if (this.counter == -2) {
            // allow character movement
            

            if (this.score > 15000 && this.score < 45000) {
                // CROW
                // variable for character speed
                this.obstacleVelocity(this.crowLeft, 600);
                this.obstacleVelocity(this.crowRight, -600);

            } else if (this.score > 50000 && this.score < 85000) {
                // GOOSE
                // variable for character speed
                this.characterSpeed = 700;
                this.obstacleVelocity(this.gooseLeft, 650);
                this.obstacleVelocity(this.gooseRight, -650);

            } else if (this.score > 90000 && this.score < 125000) {
                // HELICOPTER
                // variable for character speed
                this.characterSpeed = 750;
                this.obstacleVelocity(this.helicopterLeft, 500);
                this.obstacleVelocity(this.helicopterRight, -500);
    
            } else if (this.score > 130000 && this.score < 165000) {
                // AEROPLANE
                // variable for character speed
                this.characterSpeed = 800;
                this.obstacleVelocity(this.aeroplaneLeft, 650);
                this.obstacleVelocity(this.aeroplaneRight, -650);
    
            } else if (this.score > 170000 && this.score < 205000) {
                // FIGHTER JET
                // variable for character speed
                this.characterSpeed = 900;
                this.obstacleVelocity(this.fighterJetLeft, 800);
                this.obstacleVelocity(this.fighterJetRight, -800);

            } else if (this.score > 210000 && this.score < 245000) {
                // ROCKET
                // variable for character speed
                this.characterSpeed = 1000;
                this.obstacleVelocity(this.rocketLeft, 1000);
                this.obstacleVelocity(this.rocketRight, -1000);

            } else if (this.score > 250000 && this.score < 285000) {
                // SATELLITE
                // variable for character speed
                this.characterSpeed = 1000;
                this.obstacleVelocity(this.satelliteLeft, 900);
                this.obstacleVelocity(this.satelliteRight, -900);

            } else if (this.score > 290000 && this.score < 325000) {
                // UFO
                // variable for character speed
                this.characterSpeed = 1100;
                this.obstacleVelocity(this.ufoLeft, 1000);
                this.obstacleVelocity(this.ufoRight, -1000);
            } 

            this.moveCharacter();
            this.menuCountdown = 20;
        }
        
        // WIN
        if (this.score >= 330000 && this.gameOver == false) {
            this.counter = -3;
            this.scoreText.setVisible(false);
            this.healthBarBorder.setVisible(false);
            this.healthBar.setVisible(false);
            this.health = 100;
            this.heart.setVisible(false);

            this.astronaut.setVelocity(0,0);
            this.firstFlame.setVelocity(0,0);
            this.secondFlame.setVelocity(0,0);

            // move astronaut upwards and fly off screen
            this.astronaut.y -= 10;
            this.firstFlame.y -= 10;
            this.secondFlame.y -= 10;

            // victory text
            this.winText = this.add.text(config.width/2-310, config.height/4.5, "You Win!", {
                font: "150px Arial",
                fill: "#00ff00"
            });
            // smaller victory text
            this.winMessage = this.add.text(config.width/2-400, config.height/2.25, "You have escaped the atmosphere!", {
                font: "50px Arial",
                fill: "#00ff00"
            });

            this.menuButton = this.add.image(config.width/2, config.height/1.5, "menu-button")
                .setScale(0.2)
                .setInteractive()
                .on(Phaser.Input.Events.GAMEOBJECT_POINTER_DOWN, () => {
                    this.scene.start("bootGame");
            });

            if (this.menuCountdown == 9) {
                this.menuCountdownText.setVisible(true);
            }
        }

        // LOSE
        else if (this.gameOver == true) {
            this.counter = -3;
            this.scoreText.setVisible(false);
            this.healthBar.setVisible(false);
            this.heart.setVisible(false);

            this.astronaut.setVelocity(0,0);
            this.firstFlame.setVelocity(0,0);
            this.secondFlame.setVelocity(0,0);

            // turn off jetpack visually
            this.firstFlame.setVisible(false);
            this.secondFlame.setVisible(false);
            // move astronaut downwards and fall off screen
            this.astronaut.y += 15;
            this.firstFlame.y += 15;
            this.secondFlame.y += 15;

            // game over text
            this.loseText = this.add.text(config.width/2-340, config.height/4.5, "You Lose!", {
                font: "150px Arial",
                fill: "red"
            });
            // you lose text
            this.loseMessage = this.add.text(config.width/2-285, config.height/2.25, "You reached " + this.score + " feet!", {
                font: "50px Arial",
                fill: "red"
            });

            this.menuButton = this.add.image(config.width/2, config.height/1.5, "menu-button")
                .setScale(0.2)
                .setInteractive()
                .on(Phaser.Input.Events.GAMEOBJECT_POINTER_DOWN, () => {
                    this.scene.start("bootGame");
            });

            if (this.menuCountdown == 9) {
                this.menuCountdownText.setVisible(true);
            }
        }


    }


}