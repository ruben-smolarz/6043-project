class menu extends Phaser.Scene {
    constructor() {
        super("bootGame");
    }

    preload() {
        // preload images
        this.load.image("background", "assets/png/background.png");
        this.load.image("mountain", "assets/png/mountain.png");
        this.load.image("ground", "assets/png/ground.png");
        this.load.image("flame", "assets/png/flame.png");
        this.load.image("astronaut", "assets/png/astronaut.png");
        this.load.image("health-bar-border", "assets/png/health-bar-border.png");
        this.load.image("crow", "assets/png/crow.png");
        this.load.image("goose", "assets/png/goose.png");
        this.load.image("helicopter", "assets/png/helicopter.png");
        this.load.image("aeroplane", "assets/png/aeroplane.png");
        this.load.image("fighter-jet", "assets/png/fighter-jet.png");
        this.load.image("heart", "assets/png/heart.png");
        this.load.image("rocket", "assets/png/rocket.png");
        this.load.image("satellite", "assets/png/satellite.png");
        this.load.image("ufo", "assets/png/ufo.png");
        this.load.image("launch-button", "assets/png/launch-button.png");
        this.load.image("menu-button", "assets/png/menu-button.png");

    }

    create() {
        // menu text
        this.titleText = this.add.text(config.width/2-430, 100, "Escape the Atmosphere", {
            font: "80px Arial",
            fill: "yellow"
        });
        // user controls text
        this.userControlsText = this.add.text(config.width/2-400, 250, "1. Use arrow keys to move left, right, up, and down.", {
            font: "30px Arial",
            fill: "yellow"
        });
        // avoid obstacles text
        this.avoidObstaclesText = this.add.text(config.width/2-400, 300, "2. Avoid hitting obstacles and pickup hearts to sustain health.", {
            font: "30px Arial",
            fill: "yellow"
        });
        // game aim text
        this.gameAimText = this.add.text(config.width/2-400, 350, "3. Reach 330,000 feet to escape the atmosphere and win.", {
            font: "30px Arial",
            fill: "yellow"
        });
        // click launch text
        this.clickLaunchText = this.add.text(config.width/2-400, 400, "4. Click launch to begin the ascent!", {
            font: "30px Arial",
            fill: "yellow"
        });
        // launch button
        this.launchButton = this.add.image(config.width/2, config.height/1.3, "launch-button")
            .setScale(0.4)
            .setInteractive()
            .on(Phaser.Input.Events.GAMEOBJECT_POINTER_DOWN, () => {
                this.scene.start("playGame");
            });
    }
}