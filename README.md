# 6043-project - Escape the Atmosphere

This is a repository containing all the code from my final project for the audiovisual module (COM6043). This project is a browser game called `Escape the Atmosphere` which is written primarily in JavaScript and makes use of the Phaser 3 game framework.

+ The game is instantiated from `game.js`.
+ The menu scene code can be found in `menu.js`.
+ The play scene code can be found in `play.js`.
+ The game assets can be accessed from the `assets` directory within either of the `piskel` or `png` subdirectories.
+ The Phaser 3 framework can be found in `phaser.min.js`.
+ The website code can be found in `index.html`, `assets/style.css`, and `main.js`.

## Getting Started

These instructions will get you a copy of this project up and running on your local machine.

### Prerequisites

Before running this project, you need to download the following things:
+ [Visual Studio Code](https://code.visualstudio.com/download).
+ [Liver Server Extension](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) which can also be installed from within Visual Studio Code.

Click on each of the above for the corresponding download link.

You will also need a cloned version of this repository on your machine. You can clone the repository from here: https://gitlab.com/NickDucker/6043-project

## Viewing the Code and Making Changes

In order to view the website and play the game in your browser you will need to right-click `index.html` (at the root directory), in the `Visual Studio Code File Explorer`, and click the `Open with Live Server` option. Alternatively, the hosted version of the website can be viewed [here](https://nickducker.gitlab.io/6043-project/).

When making changes to the code, save the modified file and reload the web page to apply the changes. When the web page has reloaded, open the `Developer tools` in `Google Chrome` on the same page by pressing the `F12` key or using the `Ctrl + Shift + I` shortcut. In `Developer tools` navigate to the `Network` tab and make sure that `Disable cache` box is checked to ensure that any changes are visible upon reload.

## Built With

+ **Windows 11** - The operating system used
+ **HTML** - The markup language for displaying the project in the browser
+ **CSS** - The style sheet language for styling HTML elements
+ **JavaScript** - The main programming language
+ **Phaser** - The JavaScript game framework
+ **GitLab Pages** - The service used to publish my website directly from this repository

## Authors

+ **Nick Ducker** - [Leeds Trinity University](https://www.leedstrinity.ac.uk/)