// basic characteristics of game
var config = {
    width: 1000,
    height: 800,
    backgroundColor: 0x000000,
    // prevent random flashing lines appearing
    roundPixels: true,
    scene: [menu, play],
    pixelArt: true,
    physics: {
        default: "arcade",
        arcade: {
            debug: true
        }
    },
    parent: 'game',
    autoCenter: true
    
}

window.onload = function() {
    // instantiate game and pass config object
    new Phaser.Game(config);
}